from kivy.uix.button import Button


class CustomButton(Button):
    def __init__(self):
        super().__init__()
        self.text = ''
        self.id = 0
