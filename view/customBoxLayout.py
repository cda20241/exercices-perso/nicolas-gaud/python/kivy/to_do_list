from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.checkbox import CheckBox
from model.task import Task


class CustomBoxLayout(BoxLayout):
    def __init__(self, taskPrm: Task, **kwargs):
        super(CustomBoxLayout, self).__init__(**kwargs)

        self.task = taskPrm
        self.id = self.task.id

        # Create a CheckBox and a Label
        self.checkbox = CheckBox()
        self.label = Label(text=self.task.name)
        self.state = self.task.state

        # Add CheckBox and Label to the layout
        self.add_widget(self.checkbox)
        self.add_widget(self.label)
