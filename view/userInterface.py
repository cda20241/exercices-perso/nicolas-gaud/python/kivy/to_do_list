from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from util.util import Util
from view.customButton import CustomButton
from model.task import Task

Window.size = (400, 400)


class UserInterface:

    def __init__(self):
        self.rootLayout = None

        self.buttonsLayout = None

        self.listsLayout = None
        self.labelDone = None
        self.labelTodo = None

        self.todoListLayout = None
        self.doneListLayout = None

        self.inputBox = None

        self.stringButtons = ['Add task', 'Delete task', 'Finish task']
        self.buttons = []

        self.initGui()

    def initGui(self):
        self.rootLayout = GridLayout(cols=2)

        self.buttonsLayout = GridLayout(cols=1, rows=4)
        self.listsLayout = GridLayout(cols=1, rows=2, padding=5, spacing=5)

        self.todoListLayout = BoxLayout(orientation='vertical')
        self.labelTodo = Label(text='To do : ')
        self.todoListLayout.add_widget(self.labelTodo)

        self.doneListLayout = BoxLayout(orientation='vertical')
        self.labelDone = Label(text='Done : ')
        self.doneListLayout.add_widget(self.labelDone)

        self.inputBox = TextInput(multiline=True)
        self.buttonsLayout.add_widget(self.inputBox)
        i = Util.NUMERIC_ZERO
        for item in self.stringButtons:
            self.addButton(item, i)
            i += Util.NUMERIC_ONE

        self.listsLayout.add_widget(self.todoListLayout)
        self.listsLayout.add_widget(self.doneListLayout)

        self.rootLayout.add_widget(self.buttonsLayout)
        self.rootLayout.add_widget(self.listsLayout)

        return self.rootLayout

    def addButton(self, text, btnId):
        button = CustomButton()
        button.text = text
        button.id = btnId
        self.buttonsLayout.add_widget(button)
        self.buttons.append(button)

        return button
