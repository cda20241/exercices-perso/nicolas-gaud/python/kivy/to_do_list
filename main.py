from kivy.app import App

from controller.controller import Controller


class MyApp(App):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.rootLayout = None
        self.controller = Controller()

    def build(self):
        self.title = 'Calc by Nio'
        return self.controller.ui_instance.rootLayout


if __name__ == '__main__':
    MyApp().run()
