from kivy.storage.jsonstore import JsonStore
from model.task import Task


class Util:
    NUMERIC_MINUS_ONE = -1
    NUMERIC_ZERO = 0
    NUMERIC_ONE = 1

    def __init__(self):
        self.store = JsonStore('mydata.json')

    def doStore(self, task):
        # Convert the object to a dictionary
        taskDict = {'name': task.name, 'id': task.id, 'state': task.state}

        # Use JsonStore to store the dictionary
        self.store.put(key=task.id, data=taskDict)

    def doRetrieve(self, key):
        values = self.store.store_get(key)
        task = Task(str(values['data']['id']), values['data']['name'], str(values['data']['state']))
        return task

    def doRemove(self, key):
        self.store.delete(key)
