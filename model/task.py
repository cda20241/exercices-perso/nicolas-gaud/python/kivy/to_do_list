class Task:
    STATE_TODO = 0
    STATE_DONE = 1

    def __init__(self, taskId, name, state):
        super().__init__()
        self.name = name
        self.id = taskId
        self.state = state

    def renderTask(self):
        return f"ID: {str(self.id)}, Name: {self.name}, State: {str(self.state)}"
