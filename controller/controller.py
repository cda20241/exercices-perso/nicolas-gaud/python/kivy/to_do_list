from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label

from model.task import Task
from util.util import Util
from view.userInterface import UserInterface
from view.customBoxLayout import CustomBoxLayout


class Controller:
    EMPTY = ''
    taskIndex = Util.NUMERIC_ZERO

    def __init__(self):
        super().__init__()
        self.ui_instance = UserInterface()
        self.bindButtons(self.ui_instance.buttons)
        self.listTasks = []
        self.util = Util()

    def on_press(self, button):
        self.updateGui(button)

    def bindButtons(self, buttons):
        """
        This function binds buttons from a list of buttons to an event and an action
        :param buttons: List<Button>
        :return:
        """
        for button in buttons:
            button.bind(on_press=self.on_press)
        return buttons

    def updateGui(self, button):
        """
        This function updates the GUI depending on the button id
        :param button: Button
        :return: None
        """
        match button.id:
            case 0:
                # Adding a task
                if self.ui_instance.inputBox.text != self.EMPTY:
                    self.addTask()
            case 1:
                # Removing task either in todoList and in doneList if in
                childId = Util.NUMERIC_MINUS_ONE
                for child in self.ui_instance.todoListLayout.children:
                    if isinstance(child, CustomBoxLayout):
                        for item in child.children:
                            if isinstance(item, CheckBox) and item.active:
                                childId = child.id
                for child in self.ui_instance.doneListLayout.children:
                    if isinstance(child, CustomBoxLayout):
                        for item in child.children:
                            if isinstance(item, CheckBox) and item.active:
                                childId = child.id
                # Removing task from the list of tasks
                if childId != Util.NUMERIC_MINUS_ONE:
                    self.removeTask(childId)
            case 2:
                # Updating task
                for child in self.ui_instance.todoListLayout.children:
                    if isinstance(child, CustomBoxLayout):
                        for item in child.children:
                            if isinstance(item, CheckBox) and item.active:
                                self.updateTask(child.id)

    def addTask(self):
        """
        This function adds a task to list of tasks, the layout and store it into a Json file
        :return: Task
        """
        task = Task(self.taskIndex, self.ui_instance.inputBox.text, Task.STATE_TODO)
        self.listTasks.append(task)
        self.ui_instance.todoListLayout.add_widget(CustomBoxLayout(task))
        self.ui_instance.inputBox.text = ''
        self.taskIndex += Util.NUMERIC_ONE
        self.util.doStore(task)
        print("Task : " + task.renderTask() + " added to list, store and GUI")
        return task

    def removeTask(self, childId):
        """
        This function removes a task from the UI, from the list and deletes it from Json file
        :param childId: int
        :return: bool
        """
        isRemoved = False
        for task in self.listTasks:
            if childId == task.id:
                self.listTasks.remove(task)
                # Checking if task is in todoList, if so, removing it
                try:
                    for child in self.ui_instance.todoListLayout.children:
                        if not isinstance(child, Label) and child.id == childId:
                            childTodo = child
                            self.ui_instance.todoListLayout.remove_widget(childTodo)
                            isRemoved = True
                except IndexError:
                    print('No child in todo list to remove with this id !')
                # Checking if task is in doneList, if so, removing it
                try:
                    for child in self.ui_instance.doneListLayout.children:
                        if not isinstance(child, Label) and child.id == childId:
                            childDone = child
                            self.ui_instance.doneListLayout.remove_widget(childDone)
                            isRemoved = True
                except IndexError:
                    print('No child in done list to remove with this id !')
                if isRemoved:
                    self.util.doRemove(childId)
                    print('Task : ' + task.renderTask() + ' removed from list, store and GUI')
        return isRemoved

    def updateTask(self, childId):
        """
        This function updates a task by setting its state to done and moving in from todoList to doneList
        List of task and Json file are also updated
        :param childId: int
        :return: bool
        """
        isUpdated = False
        try:
            for child in self.ui_instance.todoListLayout.children:
                # Checking if child is not a Label and if ids are equals
                if not isinstance(child, Label) and child.id == childId:
                    childToUpdate = child
                    task = childToUpdate.task
                    for taskTmp in self.listTasks:
                        if taskTmp.id == task.id:
                            # Removing task from list of tasks and Json file
                            self.listTasks.remove(taskTmp)
                            self.util.doRemove(childId)
                    # Updating task state from 'to do' to 'done'
                    childToUpdate.state = Task.STATE_DONE
                    task.state = Task.STATE_DONE
                    # Saving task in list of tasks ans Json file
                    self.listTasks.append(task)
                    self.util.doStore(task)
                    # Removing task from todoList
                    self.ui_instance.todoListLayout.remove_widget(childToUpdate)
                    # Adding task to doneList after setting checkBox to unchecked
                    if childToUpdate is not None:
                        for item in childToUpdate.children:
                            if isinstance(item, CheckBox):
                                item.active = False
                        self.ui_instance.doneListLayout.add_widget(childToUpdate)
                        isUpdated = True
                        print('Task : ' + task.renderTask() + ' updated in list, store and GUI')
        except IndexError:
            print('No child in todo list to update with this id !')
        return isUpdated
